function romanToInt(s) {
    const romanToIntMap = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000
    };

    let result = 0;

    for (let i = 0; i < s.length; i++) {
        const currentRoman = s[i];
        const nextRoman = s[i + 1];

        if (romanToIntMap[currentRoman] < romanToIntMap[nextRoman]) {
            result -= romanToIntMap[currentRoman];
        } else {
            result += romanToIntMap[currentRoman];
        }
    }

    return result
}

const s = "III";
console.log(romanToInt(s));