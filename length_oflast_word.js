function lengthOfLastWord(s){
    let length = 0;
    let end = s.length - 1;

    while(end >= 0 && s[end] === ' '){
        end--;
    }

    while(end >= 0 && s[end] !== ' '){
        length++;
        end--;
    }

    return length;
}

const s1 = "Hello World";
console.log(lengthOfLastWord(s1));