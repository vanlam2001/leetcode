function titleToNumber(columnTitle) {
    let result = 0;

    for (let i = 0; i < columnTitle.length; i++) {
        let num = columnTitle.charCodeAt(i) - 64;

        result += num * Math.pow(26, columnTitle.length - i - 1);
    }

    return result;
}

console.log(titleToNumber("A"))