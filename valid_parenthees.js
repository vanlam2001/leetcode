function isValid(s){
    const stack = [];
    const pairs = {
        '(': ')',
        '[': ']',
        '{': '}'
    };

    for(let i = 0; i < s.length; i++){
        const char = s[i];
        if(char in pairs){
            stack.push(char);
        } else{
            const top = stack.pop();
            if (pairs[top] !== char) {
                return false; 
            }
        }
    }

    return stack.length === 0;
} 

const str1 = "()[]{}";
const str2 = "([)]";
console.log(isValid(str1)); 
console.log(isValid(str2)); 