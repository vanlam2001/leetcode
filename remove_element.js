function removeElement(nums, val){
    let k = 0;

    for(let i = 0; i < nums.length; i ++){
        if(nums[i] !== val){
            nums[k++] = nums[i]
        }
    }

    return k
}

const nums1 = [3, 2, 2, 3];
console.log(removeElement(nums1, 3));

const nums2 = [0, 1, 2, 2, 3, 0, 4, 2];
console.log(removeElement(nums2, 2));