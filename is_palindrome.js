function isPalindrome(x) {
    if (x < 0 || (x % 10 === 0 && x !== 0)) {
        return false
    }

    let reversed = 0;
    let original = x;

    while (x > 0) {
        const digit = x % 10;
        reversed = reversed * 10 + digit;
        x = parseInt(x / 10);
    }

    return original === reversed;
}

const x = 121;
console.log(isPalindrome(x));