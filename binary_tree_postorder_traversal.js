function postorderTraversal(root) {
    const result = [];

    const traverse = (node) => {
        if (node === null) {
            return;
        }

        traverse(node.left);
        traverse(node.right);
        result.push(node.val);
    };

    traverse(root);

    return result;
}

class TreeNode {
    constructor(val = 0, left = null, right = null) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}


const root = new TreeNode(1, null, new TreeNode(2, new TreeNode(3)));
console.log(postorderTraversal(root));