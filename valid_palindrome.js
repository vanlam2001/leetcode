function isPalindrome(s){
    const cleanString = s.toLowerCase().replace(/[^a-z0-9]/g, '');

    return cleanString === cleanString.split('').reverse().join('');
}

const s1 = "A man, a plan, a canal: Panama";
console.log(isPalindrome(s1));

const s2 = "A man, a plan, a canal: Panama";
console.log(isPalindrome(s2));