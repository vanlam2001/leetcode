function isSameTree(p,q){
     if(p === null && q === null){
        return true;
     }
     if(p === null || q === null){
        return false;
     } 
     if(p.val !== q.val){
        return false;
     }

     return isSameTree(p.left, q.left) && isSameTree(p.right, q.right)
}

class TreeNode {
    constructor(val = 0, left = null, right = null) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

const p1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
const q1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
console.log(isSameTree(p1, q1)); // Kết quả mong đợi là true

const p2 = new TreeNode(1, new TreeNode(2), null);
const q2 = new TreeNode(1, null, new TreeNode(2));
console.log(isSameTree(p2, q2));